package com.example.testanlyze;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MyActivity extends Activity {
    static {
        System.loadLibrary("friso");
    }

    public native String parse(String sentences);

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        ((TextView) findViewById(R.id.test)).setText(parse("what?"));

    }
}
