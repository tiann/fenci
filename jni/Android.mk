LOCAL_PATH := $(call my-dir)
##############################
include $(CLEAR_VARS)
LOCAL_MODULE    := friso
LOCAL_SRC_FILES := friso.c friso_array.c friso_hash.c friso_lexicon.c friso_link.c friso_string.c friso_ctype.c friso_UTF8.c friso_GBK.c myjni.c
LOCAL_LDLIBS    := -llog
#LOCAL_FORCE_STATIC_EXECUTABLE := true
include $(BUILD_SHARED_LIBRARY)
