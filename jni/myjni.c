#include <jni.h>

#include <android/log.h>

#include "friso.h"
#include "friso_API.h"
#include <string.h>

JNIEXPORT jstring JNICALL Java_com_example_testanlyze_MyActivity_parse(JNIEnv *env, jobject thisObj, jstring s){
    friso_t friso;
    friso_config_t config;
    friso_task_t task;
    fstring __path__ = "/sdcard/analyze/friso.ini";
     //initialize
        friso = friso_new();
        config = friso_new_config();
        friso_mode_t mode;
        if ( friso_init_from_ifile(friso, config, __path__) != 1 ) {
        __android_log_print(ANDROID_LOG_INFO, "myjni", "fail to initialize friso and config.");
    	goto err;
        }
        //friso->mode = __FRISO_SIMPLE_MODE__;
        //printf("clr_stw=%d\n", friso->clr_stw);
        //printf("match c++?%d\n", friso_dic_match( friso->dic, __LEX_ENPUN_WORDS__, "c++" ));
        //printf("match(研究)?%d\n", friso_dic_match( friso->dic, __LEX_CJK_WORDS__, "研究"));


        __android_log_print(ANDROID_LOG_INFO, "myjni", "+-Version: %s (%s)\n", friso_version(), friso->charset == FRISO_UTF8 ? "UTF-8" : "GBK");


        //set the task.
        task = friso_new_task();



    	//for ( i = 0; i < 1000000; i++ ) {
    	//set the task text.
    	friso_set_text( task, "歧义和同义词:研究生命起源，混合词: 做B超检查身体，x射线本质是什么，今天去奇都ktv唱卡拉ok去，哆啦a梦是一个动漫中的主角，单位和全角: 2009年８月６日开始大学之旅，岳阳今天的气温为38.6℃, 也就是101.48℉, 英文数字: bug report chenxin619315@gmail.com or visit http://code.google.com/p/jcseg, we all admire the hacker spirit!特殊数字: ① ⑩ ⑽ ㈩.");
    	println("分词结果:");

        __android_log_print(ANDROID_LOG_INFO, "myjni", "分词结果:");
    	while ( ( friso_next( friso, config, task ) ) != NULL ) {
    	    //printf("%s[%d, %d] ", task->hits->word,
    	    //	    task->hits->offset, task->hits->length );
    	    __android_log_print(ANDROID_LOG_INFO, "myjni", "%s[%d, %d] ", task->hits->word,
                                                               	    	    task->hits->offset, task->hits->length);

    	}


        friso_free_task( task );
//        friso_free_config(config);
//        friso_free(friso);
    err:
        friso_free_config(config);
        friso_free(friso);
    return (*env)->NewStringUTF(env, "Hello!!");
}

